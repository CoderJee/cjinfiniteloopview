//
//  ViewController.m
//  InfiniteLoopScrollDemo
//
//  Created by iOS_Developer on 15/12/29.
//  Copyright © 2015年 CoderJee. All rights reserved.
//

#import "ViewController.h"
#import "CJInfiniteLoopView.h"
#import "Common.h"
@interface ViewController ()<CJInfiniteLoopViewDataSource>

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    CJInfiniteLoopView *infiniteLoopView = [[CJInfiniteLoopView alloc] initWithCustomPageControl:nil];
    //infiniteLoopView.delegate = self;
    infiniteLoopView.dataSource = self;
    infiniteLoopView.pageIndicatorTintColor = [UIColor blueColor];
    infiniteLoopView.currentPageIndicatorTintColor = [UIColor redColor];
    infiniteLoopView.frame = CGRectMake(0, 0, self.view.width, 200);
    [self.view addSubview:infiniteLoopView];
    infiniteLoopView.timeInterval = 0.5;
    
}

- (NSInteger)numberOfItemsInLoopView:(CJInfiniteLoopView *)loopView
{
    return 3;
}

- (UIView *)infiniteLoopView:(CJInfiniteLoopView *)loopView viewForItemAtIndex:(NSInteger)index
{
//    UIView *view = [[UIView alloc] init];
//    view.backgroundColor = RandomColor;
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"%ld",index + 1]]];
    return imageView;
}

@end
