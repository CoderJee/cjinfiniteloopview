//
//  AppDelegate.h
//  InfiniteLoopScrollDemo
//
//  Created by iOS_Developer on 15/12/29.
//  Copyright © 2015年 CoderJee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

