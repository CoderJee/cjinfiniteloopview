//
//  CJInfiniteLoopView.h
//  InfiniteLoopScrollDemo
//
//  Created by iOS_Developer on 15/12/29.
//  Copyright © 2015年 CoderJee. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CJInfiniteLoopView;

@protocol CJInfiniteLoopViewDelegate <NSObject>

- (void)infiniteLoopView:(nonnull CJInfiniteLoopView *)loopView didSelectItemAtIndex:(NSInteger)index;

@end

@protocol CJInfiniteLoopViewDataSource <NSObject>

- (NSInteger)numberOfItemsInLoopView:(nonnull CJInfiniteLoopView *)loopView;

- (UIView *)infiniteLoopView:(nonnull CJInfiniteLoopView *)loopView viewForItemAtIndex:(NSInteger)index;

@end

@interface CJInfiniteLoopView : UIView

@property (nonatomic, weak)id<CJInfiniteLoopViewDelegate> delegate;

@property (nonatomic, weak)id<CJInfiniteLoopViewDataSource> dataSource;
/**
 *  如果customPageControl为nil,则使用系统自带UIPageControl.
 */
- (instancetype)initWithCustomPageControl:(UIPageControl *)customPageControl;

@property(nullable, nonatomic,strong) UIColor *pageIndicatorTintColor NS_AVAILABLE_IOS(6_0) UI_APPEARANCE_SELECTOR;

@property(nullable, nonatomic,strong) UIColor *currentPageIndicatorTintColor NS_AVAILABLE_IOS(6_0) UI_APPEARANCE_SELECTOR;

/**
 *  设置InfiniteLoopView的背景色
 */
@property (nonatomic, weak)UIColor *backgroundColor;
/**
 *  PageControl的大小
 */
@property (nonatomic, assign)CGSize pageSize;
/**
 *  PageControl 距离LoopView左边的距离
 */
@property (nonatomic, strong)NSNumber *leftInset;
/**
 *  PageControl 距离LoopView右边的距离
 */
@property (nonatomic, strong)NSNumber *rightInset;
/**
 *  PageControl 距离LoopView下边的距离
 */
@property (nonatomic, strong)NSNumber *bottomInset;
/**
 *  PageControl 距离LoopView上边的距离
 */
@property (nonatomic, strong)NSNumber *topInset;
/**
 *  滚动时间间隔
 */
@property (nonatomic, assign)float timeInterval;

- (void)reloadData;

@end
