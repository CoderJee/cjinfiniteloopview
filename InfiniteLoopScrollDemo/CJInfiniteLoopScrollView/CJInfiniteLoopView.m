//
//  CJInfiniteLoopView.m
//  InfiniteLoopScrollDemo
//
//  Created by iOS_Developer on 15/12/29.
//  Copyright © 2015年 CoderJee. All rights reserved.
//

#import "CJInfiniteLoopView.h"
#import "Common.h"
static const int maxSections = 200;

static const float pageDefaultHeight = 10;

static const float bottomPadding = 5;

#define PAGE_DEFAULTWIDHT(count) count * 16

#define IDENTIFIER  @"InfiniteLoopCollectionViewCell"

@interface CJInfiniteLoopView()<UICollectionViewDelegate, UICollectionViewDataSource>
@property (nonatomic, weak)UICollectionView *collectionView;
@property (nonatomic, weak)UICollectionViewFlowLayout *layout;
@property (nonatomic, strong)UIPageControl *pageControl;
@property (nonatomic, strong) NSTimer *timer;

@end

@implementation CJInfiniteLoopView

- (instancetype)init
{
    if (self = [super init]) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        layout.minimumInteritemSpacing = 0;
        layout.minimumLineSpacing = 0;
        _layout = layout;
        [self setUpCollectionViewWithLayout:layout];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.itemSize = CGSizeMake(frame.size.width, frame.size.height);
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        layout.minimumInteritemSpacing = 0;
        layout.minimumLineSpacing = 0;
        _layout = layout;
        
        [self setUpCollectionViewWithLayout:layout];
    }
    return self;
}

- (void)setUpCollectionViewWithLayout:(UICollectionViewFlowLayout *)layout
{
    UICollectionView *collectionView;
    if (layout != nil) {
        collectionView = [[UICollectionView alloc] initWithFrame:self.bounds collectionViewLayout:layout];
    }else
        collectionView = [[UICollectionView alloc] init];
    collectionView.showsHorizontalScrollIndicator = NO;
    collectionView.pagingEnabled = YES;
    collectionView.delegate = self;
    collectionView.dataSource = self;
    [collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:IDENTIFIER];
    [self addSubview:collectionView];
    [collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.bottom.right.mas_equalTo(self);
    }];
    _collectionView = collectionView;
}

- (instancetype)initWithCustomPageControl:(UIPageControl *)customPageControl
{
    if (self = [super init]) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        layout.minimumInteritemSpacing = 0;
        layout.minimumLineSpacing = 0;
        _layout = layout;
        [self setUpCollectionViewWithLayout:layout];
        if (customPageControl == nil) {
            UIPageControl *pageControl = [[UIPageControl alloc] init];
            _pageControl = pageControl;
        }else
            _pageControl = customPageControl;
        [self addSubview:_pageControl];
    }
    return  self;
}

- (void)setBackgroundColor:(UIColor *)backgroundColor
{
    self.backgroundColor = backgroundColor;
    self.collectionView.backgroundColor = backgroundColor;
}

- (void)setLeftInset:(NSNumber *)leftInset
{
    _leftInset = leftInset;
    if (leftInset != nil && self.pageControl != nil) {
        
        [self.pageControl mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.mas_left).with.offset(leftInset.floatValue);
            if (self.bottomInset != nil) {
                make.bottom.mas_equalTo(self.mas_bottom).with.offset(-self.bottomInset.floatValue);
            }else if(self.topInset != nil)
                make.top.mas_equalTo(self.mas_top).with.offset(self.topInset.floatValue);
            else
                make.bottom.mas_equalTo(self.mas_bottom).with.offset(-bottomPadding);

            if (self.pageSize.height == 0 && self.pageSize.width == 0) {
                make.size.mas_equalTo(CGSizeMake(PAGE_DEFAULTWIDHT(1), pageDefaultHeight));
            }else
                make.size.mas_equalTo(self.pageSize);
        }];
        [self.pageControl layoutIfNeeded];
    }
}

- (void)setRightInset:(NSNumber *)rightInset
{
    _rightInset = rightInset;
    if (rightInset != nil && self.pageControl != nil) {
        
        [self.pageControl mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.mas_right).with.offset(-rightInset.floatValue);
            if (self.bottomInset != nil) {
                make.bottom.mas_equalTo(self.mas_bottom).with.offset(-self.bottomInset.floatValue);
            }else if(self.topInset != nil)
                make.top.mas_equalTo(self.mas_top).with.offset(self.topInset.floatValue);
            else
                make.bottom.mas_equalTo(self.mas_bottom).with.offset(-bottomPadding);
                
            if (self.pageSize.height == 0 && self.pageSize.width == 0) {
                make.size.mas_equalTo(CGSizeMake(PAGE_DEFAULTWIDHT(1), pageDefaultHeight));
            }else
                make.size.mas_equalTo(self.pageSize);
        }];
        [self.pageControl layoutIfNeeded];
    }
}

- (void)setBottomInset:(NSNumber *)bottomInset
{
    _bottomInset = bottomInset;
    if (bottomInset != nil && self.pageControl != nil) {
        [self.pageControl mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(self.mas_bottom).with.offset(-bottomInset.floatValue);
            if (self.leftInset != nil) {
                
                make.left.mas_equalTo(self.mas_left).with.offset(self.leftInset.floatValue);
            }else if(self.rightInset != nil){
                
                make.right.mas_equalTo(self.mas_right).with.offset(-self.rightInset.floatValue);
            }else{
                
                make.centerX.mas_equalTo(self.mas_centerX);
            }
            if (self.pageSize.height == 0 && self.pageSize.width == 0) {
                make.size.mas_equalTo(CGSizeMake(PAGE_DEFAULTWIDHT(1), pageDefaultHeight));
            }else
                make.size.mas_equalTo(self.pageSize);
        }];
        [self.pageControl layoutIfNeeded];
    }
}

- (void)setTopInset:(NSNumber *)topInset
{
    _topInset = topInset;
    if (topInset != nil && self.pageControl != nil) {
        [self.pageControl mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.mas_top).with.offset(-topInset.floatValue);
            if (self.leftInset != nil) {
                make.left.mas_equalTo(self.mas_left).with.offset(self.leftInset.floatValue);
                
            }else if(self.rightInset != nil){
                make.right.mas_equalTo(self.mas_right).with.offset(-self.rightInset.floatValue);
                
            }else{
                make.centerX.mas_equalTo(self.mas_centerX);
                
            }
            if (self.pageSize.height == 0 && self.pageSize.width == 0) {
                make.size.mas_equalTo(CGSizeMake(PAGE_DEFAULTWIDHT(1), pageDefaultHeight));
            }else
                make.size.mas_equalTo(self.pageSize);
        }];
        [self.pageControl layoutIfNeeded];
    }
}

- (void)setPageSize:(CGSize)pageSize
{
    _pageSize = pageSize;
    if ((self.pageSize.height != 0 || self.pageSize.width != 0) && self.pageControl != nil) {
        [self.pageControl mas_remakeConstraints:^(MASConstraintMaker *make) {
            if (self.leftInset != nil) {
                make.left.mas_equalTo(self.mas_left).with.offset(self.leftInset.floatValue);
                
            }else if(self.rightInset != nil){
                make.right.mas_equalTo(self.mas_right).with.offset(-self.rightInset.floatValue);
                
            }else{
                make.centerX.mas_equalTo(self.mas_centerX);
                
            }
            
            if (self.bottomInset != nil) {
                make.bottom.mas_equalTo(self.mas_bottom).with.offset(-self.bottomInset.floatValue);
            }else if(self.topInset != nil)
                make.top.mas_equalTo(self.mas_top).with.offset(self.topInset.floatValue);
            else
                make.bottom.mas_equalTo(self.mas_bottom).with.offset(-bottomPadding);
            make.size.mas_equalTo(self.pageSize);
        }];
    }
    
}

- (void)setPageIndicatorTintColor:(UIColor *)pageIndicatorTintColor
{
    _pageIndicatorTintColor = pageIndicatorTintColor;
    self.pageControl.pageIndicatorTintColor = pageIndicatorTintColor;
}

- (void)setCurrentPageIndicatorTintColor:(UIColor *)currentPageIndicatorTintColor
{
    _currentPageIndicatorTintColor = currentPageIndicatorTintColor;
    self.pageControl.currentPageIndicatorTintColor = currentPageIndicatorTintColor;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    if (self.dataSource != nil) {
        if ([self.dataSource numberOfItemsInLoopView:self] <= 1)
            return 1;
        else{
            if (self.pageSize.height != 0 || self.pageSize.width != 0) {
                
            }else
                [self setPageSize:CGSizeMake(PAGE_DEFAULTWIDHT([self.dataSource numberOfItemsInLoopView:self]), pageDefaultHeight)];
            self.pageControl.numberOfPages = [self.dataSource numberOfItemsInLoopView:self];
            return maxSections;
        }
    }else
        return 0;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if ([self.dataSource respondsToSelector:@selector(numberOfItemsInLoopView:)]) {
        return [self.dataSource numberOfItemsInLoopView:self];
    }else
        return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:IDENTIFIER forIndexPath:indexPath];
    [cell.contentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    UIView *view = [self.dataSource infiniteLoopView:self viewForItemAtIndex:indexPath.row];
    [cell.contentView addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.bottom.right.mas_equalTo(cell.contentView);
    }];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.delegate respondsToSelector:@selector(infiniteLoopView:didSelectItemAtIndex:)]) {
        [self.delegate infiniteLoopView:self didSelectItemAtIndex:indexPath.row];
    }
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    if (self.layout == nil) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        layout.minimumInteritemSpacing = 0;
        layout.minimumLineSpacing = 0;
        layout.itemSize = self.size;
        _layout = layout;
        _collectionView.collectionViewLayout = layout;
    }else
    {
        _layout.itemSize = self.size;
    }
    
}

- (void)setTimeInterval:(float)timeInterval
{
    _timeInterval = timeInterval == 0.0 ? 4.0 : timeInterval;
    [self addTimer];
}

- (void)addTimer
{
    [self.timer invalidate];
    self.timer = nil;
    float time = _timeInterval == 0.0 ? 4.0 : _timeInterval;
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:time target:self selector:@selector(nextPage) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
    self.timer = timer;
}

- (void)removeTimer
{
    // 停止定时器
    [self.timer invalidate];
    self.timer = nil;
}

- (void)nextPage
{
    if ([self.dataSource numberOfItemsInLoopView:self] <= 1) {
        return;
    }
    // 1.马上显示回最中间那组的数据
    NSIndexPath *currentIndexPathReset = [self resetIndexPath];
    
    // 2.计算出下一个需要展示的位置
    NSInteger nextItem = currentIndexPathReset.item + 1;
    NSInteger nextSection = currentIndexPathReset.section;
    if (nextItem == [self.dataSource numberOfItemsInLoopView:self]) {
        nextItem = 0;
        nextSection++;
    }
    NSIndexPath *nextIndexPath = [NSIndexPath indexPathForItem:nextItem inSection:nextSection];
    
    // 3.通过动画滚动到下一个位置
    if ([self.dataSource numberOfItemsInLoopView:self] > 1)
        [self.collectionView scrollToItemAtIndexPath:nextIndexPath atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
}

- (NSIndexPath *)resetIndexPath
{
    // 当前正在展示的位置
    NSIndexPath *currentIndexPath = [[self.collectionView indexPathsForVisibleItems] lastObject];
    // 马上显示回最中间那组的数据
    NSIndexPath *currentIndexPathReset = [NSIndexPath indexPathForItem:currentIndexPath.item inSection:[self numberOfSectionsInCollectionView:_collectionView] / 2];
    if (![self.dataSource numberOfItemsInLoopView:self] == 0) {
        
        [self.collectionView scrollToItemAtIndexPath:currentIndexPathReset atScrollPosition:UICollectionViewScrollPositionLeft animated:NO];
    }
    return currentIndexPathReset;
}

- (void)willMoveToSuperview:(UIView *)newSuperview
{
    [self reloadData];
}

- (void)dealloc
{
    [self removeTimer];
}

- (void)reloadData
{
    [self.collectionView reloadData];
    [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:[self numberOfSectionsInCollectionView:_collectionView] / 2] atScrollPosition:UICollectionViewScrollPositionLeft animated:NO];
    [self addTimer];
}

/**
 *  当用户即将开始拖拽的时候就调用
 */
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self removeTimer];
}

/**
 *  当用户停止拖拽的时候就调用
 */
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self addTimer];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    int page = (int)(scrollView.contentOffset.x / scrollView.bounds.size.width + 0.5) % [self.dataSource numberOfItemsInLoopView:self];
    
    self.pageControl.currentPage = page;
}


@end
