//
//  Common.h
//  InfiniteLoopScrollDemo
//
//  Created by iOS_Developer on 15/12/29.
//  Copyright © 2015年 CoderJee. All rights reserved.
//

#ifndef Common_h
#define Common_h

#import "Masonry.h"
#import "UIColor+Extension.h"
#import "UIView+Extension.h"

// 颜色
#define CJRGBColor(r, g, b) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1.0]
#define RandomColor CJRGBColor((arc4random()%255),(arc4random()%255),(arc4random()%255)) ;


#endif /* Common_h */

