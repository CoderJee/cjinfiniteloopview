//
//  UIColor+Extension.h
//  InfiniteLoopScrollDemo
//
//  Created by iOS_Developer on 15/12/29.
//  Copyright © 2015年 CoderJee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Extension)

/**
 *  将16进制的颜色直接转成可用的UIColor
 *
 *  @param color 16进制颜色
 *
 *  @return 返回的UIColor
 */
+ (UIColor *)colorWithHexString:(NSString *)color;

/**
 *  将16进制的颜色直接转成可用的UIColor并可以透明度
 *
 *  @param color 16进制颜色
 *  @param alpha 颜色透明度
 *
 *  @return 一定透明度的颜色
 */
+ (UIColor *)colorWithHexString:(NSString *)color alpha:(CGFloat)alpha;

@end
