# CJInfiniteLoopView
##一款用起来so easy的左右无限滚动控件，一般用于首页banner
![效果图](http://i11.tietuku.com/ae14c8bc29dbb554.gif)
## 使用文档:[自定义水平无限滚动控件之CJInfiniteLoopView](http://www.jianshu.com/p/d29d44f66f9b)
###可使用cocoapods导入
```
    pod 'CJInfiniteLoopView'
```
###基本使用
1.1.创建CJInfiniteLoopView
```
    CJInfiniteLoopView *infiniteLoopView = [[CJInfiniteLoopView alloc] initWithCustomPageControl:nil];
    infiniteLoopView.delegate = self;     // self需实现CJInfiniteLoopViewDelegate协议
    infiniteLoopView.dataSource = self;   // self需实现CJInfiniteLoopViewDataSource协议
    infiniteLoopView.pageIndicatorTintColor = [UIColor blueColor];
    infiniteLoopView.currentPageIndicatorTintColor = [UIColor redColor];
    infiniteLoopView.frame = CGRectMake(0, 0, self.view.width, 200);
    [self.view addSubview:infiniteLoopView];
    infiniteLoopView.timeInterval = 1.0;
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor blueColor];
    infiniteLoopView.placeHolderView = view;
```

2.设置CJInfiniteLoopView.h的数据源，实现CJInfiniteLoopViewDataSource协议，实现 *- (NSInteger)numberOfItemsInLoopView:(CJInfiniteLoopView *)loopView* 和 *- (UIView *)infiniteLoopView:(CJInfiniteLoopView *)loopView viewForItemAtIndex:(NSInteger)index* 方法
```
  
- (NSInteger)numberOfItemsInLoopView:(CJInfiniteLoopView *)loopView
{
    return 3;
}

- (UIView *)infiniteLoopView:(CJInfiniteLoopView *)loopView viewForItemAtIndex:(NSInteger)index
{
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"%ld",index + 1]]];
    return imageView;
}
```

3.如果要响应事件，则要实现CJInfiniteLoopViewDelegate协议,实现*- (void)infiniteLoopView:(CJInfiniteLoopView *)loopView didSelectItemAtIndex:(NSInteger)index* 方法
```
// 响应点击事件
- (void)infiniteLoopView:(CJInfiniteLoopView *)loopView didSelectItemAtIndex:(NSInteger)index
{
    NSLog(@"%@", loopView);
}
```
**如果不需要pageControl，则使用[[CJInfiniteLoopView alloc] init],使用initWithCustomPageControl方法创建如果传nil，则会有个默认的pageControl,也可以传自己自定义的pageControl**

#### 如有疑问，bug欢迎issue, 也可在微博上 *@埃欧Ace* 提疑问或者bug。如有bug欢迎各位指正，谢谢！